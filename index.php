<!DOCTYPE HTML>
<!--
	Industrious by TEMPLATED
	templated.co @templatedco
	Released for free under the Creative Commons Attribution 3.0 license (templated.co/license)
-->
<html>
	<head>
		<title>Oxbridge.tips </title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
		<meta name="description" content="" />
		<meta name="keywords" content="" />
		<link rel="stylesheet" href="assets/css/main.css" />
	</head>
	<body class="is-preload">

		<!-- Header -->
			<header id="header">
				<a class="logo" href="index.html">University Application Tutors </a>
				<nav>
					<a href="#menu">Menu</a>
				</nav>
			</header>

		<!-- Nav -->
			<nav id="menu">
				<ul class="links">
					<li><a href="index.html">Home</a></li>
					<!-- <li><a href="elements.html">Elements</a></li> -->
					<li><a href="schools.html">Talks & Seminars</a></li>
					<li><a href="applications.html">University Applications</a></li>
					<li><a href="tutors.html">The tutors</a></li>
					<li><a href="contact.html">Contact us</a></li>
				</ul>
			</nav>

		<!-- Banner -->
			<section id="banner">
				<div class="inner">
					<h1>Oxbridge.tips</h1>
					<p>Getting your University application right<br /></p>



				<!-- <img src="images/banner.jpg"></img> -->
				<!-- <video autoplay loop muted playsinline src="images/banner.mp4"></video> -->
			</section>

		<!-- Highlights -->
			<section class="wrapper">
				<div class="inner">
					<header class="special">
						<h2>School talks and seminars</h2>
						<p>We deliver a number of talks and seminars to help prospective university applicants choose the right course, at the right University, and submit a successful application.</p>

					</header>
					<div class="highlights">
						<section>
							<div class="content">
								<header>
									<a href="#" class="icon fa-university"><span class="label">Icon</span></a>
									<h3>How to get the most from an open day</h3>
								</header>
								<p>What to look for, what to avoid, what to expect.</p>
							</div>
						</section>

						<section>
							<div class="content">
								<header>
									<a href="#" class="icon fa-calculator"><span class="label">Icon</span></a>
									<h3>Oxbridge admissions tests, UKCAT / MCAT etc. </h3>
								</header>
								<p>For most courses at Oxbridge universities you are required to take additional tests as part of your application.</p>
							</div>
						</section>

										<section>
							<div class="content">
								<header>
									<a href="#" class="icon fa-file-text-o "><span class="label">Icon</span></a>
									<h3>The Personal Statement</h3>
								</header>
								<p>What needs to be in a personal statement and how to make it read well?.</p>
							</div>
						</section>
				</div>

					<div class="inner">
					<header class="special">
						<h2>Application Support</h2>
						<p>We can help you polish your Personal Statement and provide individual support in all other aspects of your application</p>
					</header>
					<div class="highlights">
						<section>
							<div class="content">
								<header>
									<a href="#" class="icon fa-files-o"><span class="label">Icon</span></a>
									<h3>Personal Statements</h3>
								</header>
								<p> One of our tutors will provide feedback and suggestions to help you refine your statement and make the best impression.</p>
							</div>
						</section>

						<section>
							<div class="content">
								<header>
									<a href="#" class="icon fa-users"><span class="label">Icon</span></a>
									<h3>Interview Preparation</h3>
								</header>
								<p>Oxbridge interviews all applicants and so do many of those belonging to the Russell Group. Our tutors can help you prepare and give you mock-interviews.</p>
							</div>
						</section>

						<section>
							<div class="content">
								<header>
									<a href="#" class="icon fa-superscript"><span class="label">Icon</span></a>
									<h3>Exam Coaching</h3>
								</header>
								<p>We can help you prepare for Oxbridge entrance exams and course-specific exams like the UKCAT .</p>
							</div>
						</section>

					</div>
				</div>
			</section>

		<!-- CTA -->
			<section id="cta" class="wrapper">
				<div class="inner">
					<h2>"It’s important to remember that you can only write one personal statement... Universities build a picture of you as a student from all the different information you provide, to help decide whether or not to offer you a place."</h2>
					<h3 style="text-align: :right;"> - University of Oxford Admissions </h3>
<!-- 					<p>Nunc lacinia ante nunc ac lobortis. Interdum adipiscing gravida odio porttitor sem non mi integer non faucibus ornare mi ut ante amet placerat aliquet. Volutpat eu sed ante lacinia sapien lorem accumsan varius montes viverra nibh in adipiscing. Lorem ipsum dolor vestibulum ante ipsum primis in faucibus vestibulum. Blandit adipiscing eu felis iaculis volutpat ac adipiscing sed feugiat eu faucibus. Integer ac sed amet praesent. Nunc lacinia ante nunc ac gravida.</p> -->
				</div>
			</section>

		<!-- Testimonials -->
			<section class="wrapper">
				<div class="inner">
					<header class="special">
						<h2>Testimonials</h2>
						<!-- <p>In arcu accumsan arcu adipiscing accumsan orci ac. Felis id enim aliquet. Accumsan ac integer lobortis commodo ornare aliquet accumsan erat tempus amet porttitor.</p> -->
					</header>
					<div class="testimonials">
						<section>
							<div class="content">
								<blockquote>
									<p>...suggestions were constructive and insightful, they really improved the structure of my personal statement. I would highly recommend this service if applying for further education.</p>
								</blockquote>
								<div class="author">
									<div class="image">
										<img src="images/philKing.jpeg" alt="" />
									</div>
									<p class="credit">- <strong> Phil </strong> <span>King's College applicant</span></p>
								</div>
							</div>
						</section>
						<section>
							<div class="content">
								<blockquote>
									<p> <br>I needed help with my personal statement. The feedback was outstanding value and far exceeded the input from my school. <br> <br></p>
								</blockquote>
								<div class="author">
									<div class="image">
										<img src="images/cumstain.jpeg" alt="" />
									</div>
									<p class="credit">- <strong>Philip</strong> <span>University of Edinburgh applicant</span></p>
								</div>
							</div>
						</section>
						<section>
							<div class="content">
								<blockquote>
									<p>Nunc lacinia ante nunc ac lobortis ipsum. Interdum adipiscing gravida odio porttitor sem non mi integer non faucibus.</p>
								</blockquote>
								<div class="author">
									<div class="image">
										<img src="images/pic02.jpg" alt="" />
									</div>
									<p class="credit">- <strong>Janet Smith</strong> <span>CEO - ABC Inc.</span></p>
								</div>
							</div>
						</section>
					</div>
				</div>
			</section>

		<!-- Footer -->
				<!-- <iframe src="./test.html" seamless></iframe> -->
			<footer id="footer">
				<div class="inner">
					<div class="content">
						<section>
							<h3>About us</h3>
							<p>The time you spend at University is the last of your formative years. It's so important to study the right course and have a good experience because the skills you learn and the friends you make will stay with you long after you graduate. We came together to offer bespoke admissions training because each of us received poor advice when we were preparing our own applications, but later worked for various tutoring and proofreading agencies to see how much difference some good advice can make to a University application and subsequent experience.</p>
						</section>
						<section>
							<h4>More information?</h4>
							<ul class="alt">
								<li><a href="#">Seminars and Workshops</a></li>
								<li><a href="#">Individual support</a></li>
								<li><a href="#">Our Tutors</a></li>
								<li><a href="#">Contact Us</a></li>
							</ul>
						</section>
						<section>
							<h4>Magna sed ipsum</h4>
							<ul class="plain">
								<li><a href="#"><i class="icon fa-twitter">&nbsp;</i>Twitter</a></li>
								<li><a href="#"><i class="icon fa-facebook">&nbsp;</i>Facebook</a></li>
								<li><a href="#"><i class="icon fa-instagram">&nbsp;</i>Instagram</a></li>
								<li><a href="#"><i class="icon fa-github">&nbsp;</i>Github</a></li>
							</ul>
						</section>
					</div>
					<div class="copyright">
						&copy; Christian Laurent 2020 or otherwise covered by a Creative Commons License.</a>.
					</div>
				</div>
<?php include 'test.html'; ?>

		</footer>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/browser.min.js"></script>
			<script src="assets/js/breakpoints.min.js"></script>
			<script src="assets/js/util.js"></script>
			<script src="assets/js/main.js"></script>
	</body>
</html>
