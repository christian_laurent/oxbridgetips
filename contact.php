<?php

if($_POST["message"]) {

mail("Christian_laurent@live.com", "OxTips Enquiry",

$_POST["insert your message here"]. "From: an@email.address");

}

?>
<!DOCTYPE HTML>
<!--
	Industrious by TEMPLATED
	templated.co @templatedco
	Released for free under the Creative Commons Attribution 3.0 license (templated.co/license)
-->
<html>
	<head>
		<title>OxTips Contact Us</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
		<meta name="description" content="" />
		<meta name="keywords" content="" />
		<link rel="stylesheet" href="assets/css/main.css" />
	</head>
	<body class="is-preload">

		<!-- Header -->
			<header id="header">
				<a class="logo" href="index.html">Home</a>
				<nav>
					<a href="#menu">Menu</a>
				</nav>
			</header>

		<!-- Nav -->
			<nav id="menu">
				<ul class="links">
					<li><a href="index.html">Home</a></li>
					<li><a href="elements.html">Elements</a></li>
					<li><a href="generic.html">Generic</a></li>
				</ul>
			</nav>

		<!-- Heading -->
			<div id="heading" >
				<h1>Enquiries and Bookings</h1>
			</div>

	<!-- 	<!-- Main -->
			<section id="main" class="wrapper">
				<div class="inner">
					<div class="content">
						<header>
	
									<h3>Please fill out the form below and we will make sure the correct person will get back to you as soon as possible.</h3>
									Alternatively <a href="tel:07821405026">Call us </a> on 07821405026 <br> <br>



									<form method="post" action="contactform.php">
										<div class="row gtr-uniform">
											<div class="col-6 col-12-xsmall">
												<input type="text" name="name" id="name" value="" placeholder="Name" />
											</div>
											<div class="col-6 col-12-xsmall">
												<input type="email" name="email" id="email" value="" placeholder="Email" />
											</div>
									<!-- 			<div class="col-6 col-12-xsmall">
												<input type="email" name="email" id="email" value="" placeholder="Telephone" />
											</div> -->




					
									<p> Which service are you interested in? </p><br>
											<div class="col-4 col-12-small">
												<input type="radio" id="radio-alpha" name="radio" checked>
												<label for="radio-alpha">Private Hire</label>
											</div>
											<div class="col-4 col-12-small">
												<input type="radio" id="radio-beta" name="radio">
												<label for="radio-beta">School seminars / workshops</label>
											</div>
											<!-- div class="col-4 col-12-small">
												<input type="radio" id="radio-gamma" name="radio">
												<label for="radio-gamma">Radio gamma</label>
											</div> -->
											<!-- Break -->
										<!-- 	<div class="col-6 col-12-small">
												<input type="checkbox" id="checkbox-alpha" name="checkbox">
												<label for="checkbox-alpha">Checkbox alpha</label>
											</div>
											<div class="col-6 col-12-small">
												<input type="checkbox" id="checkbox-beta" name="checkbox" checked>
												<label for="checkbox-beta">Checkbox beta</label>
											</div> -->
											<!-- Break -->
											<div class="col-12">
												<textarea name="textarea" id="textarea" placeholder="Message  " rows="6"></textarea>
											</div>
											<!-- Break -->
											<div class="col-12">
												<ul class="actions">
													<li><input type="submit" value="Submit Form" class="primary" /></li>
													<li><input type="reset" value="Reset" /></li>
												</ul>
											</div>
										</div>
									</form>


					</div>
				</div>
			</section>
 -->
		<!-- Footer -->
			<footer id="footer">
				<div class="inner">
					<div class="content">
						<section>
							<h3>About us</h3>
							<p>The time you spend at University is the last of your formative years. It's so important to study the right course and have a good experience because the skills you learn and the friends you make will stay with you long after you graduate. We came together to offer bespoke admissions training because each of us received poor advice when we were preparing our own applications. We later worked for various tutoring and proofreading agencies to see how much difference some good advice can make to a University application and subsequent experience.</p>
						</section>
						<section>
							<!-- <h4>Helpful links</h4> -->
							<ul class="plain">
							<!-- 	<li><a href="#"><i class="icon fa-twitter">&nbsp;</i>Twitter</a></li>
								<li><a href="#"><i class="icon fa-facebook">&nbsp;</i>Facebook</a></li>
								<li><a href="#"><i class="icon fa-instagram">&nbsp;</i>Instagram</a></li>
								<li><a href="#"><i class="icon fa-github">&nbsp;</i>Github</a></li> -->
							</ul>
						</section>
						<section>
							<h4>More information?</h4>
							<ul class="alt">
								<li><a href="#">Seminars and Workshops</a></li>
								<li><a href="#">University Applications</a></li>
								<li><a href="#">Our Tutors</a></li>
								<li><a href="#">Contact Us</a></li>
							</ul>
						</section>
					</div>
					<div class="copyright">
						&copy; Christian Laurent 2020 or otherwise covered by a Creative Commons License.</a>.
					</div>
				</div>


		</footer>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/browser.min.js"></script>
			<script src="assets/js/breakpoints.min.js"></script>
			<script src="assets/js/util.js"></script>
			<script src="assets/js/main.js"></script>

	</body>
</html>